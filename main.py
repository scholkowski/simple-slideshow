from PIL import Image
from moviepy.editor import *
import os
import random

background_path_1 = "background.png"
background_path_2 = "background_mirrored.png"
content_folders = ["Bilder", "Werbung"]
duration_per_image = 4

# Set max content size (Pixel)
MAX_CONTENT_HEIGHT = 480
MAX_CONTENT_WIDTH = 1000

# Create list of all elements
contents = []
formats = ["bmp", "png", "jpg", "jpeg"]
for directory in content_folders:
    for filename in os.listdir(directory):
        if (os.path.isfile(os.path.join(directory, filename))) and (len(filename.split(".")) == 2) and (
                filename.split(".")[1].lower() in formats):
            contents.append(os.path.join(directory, filename))

# Randomize contents
random.shuffle(contents)

image_clips = []
i = 1
for file_path in contents:
    print("Image " + str(i) + ": " + file_path)

    # Alternate between backgrounds
    if i % 2:
        background = Image.open(background_path_1)
    else:
        background = Image.open(background_path_2)

    content = Image.open(file_path)

    # Resize content to fit on background
    height_ratio = MAX_CONTENT_HEIGHT / content.height
    width_ratio = MAX_CONTENT_WIDTH / content.width

    if height_ratio < 1 or width_ratio < 1:
        if height_ratio < width_ratio:
            content = content.resize((int(content.width * height_ratio), int(content.height * height_ratio)))
        else:
            content = content.resize((int(content.width * width_ratio), int(content.height * width_ratio)))

    # Paste content on background centered
    background.paste(content,
                     (int(background.width / 2 - content.width / 2), int(background.height / 2 - content.height / 2)))

    # Save joined image
    if not os.path.exists("out"):
        os.mkdir("out")

    # Save images to out/ and create ImageClip from current image
    out_path_img = os.path.join("out", "img" + str(i).zfill(3) + ".png")
    background.save(out_path_img, "PNG")
    image_clips.append(ImageClip(out_path_img).set_duration(duration_per_image))
    i += 1

# Save slideshow to out/slideshow.mp4
out_path_video = os.path.join("out", "slideshow.mp4")
slideshow_video = concatenate(image_clips, method="compose")
slideshow_video.write_videofile(out_path_video, fps=24)
